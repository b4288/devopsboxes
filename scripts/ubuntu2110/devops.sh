#!/bin/bash -x

# Install arkade tools.
curl -SLfs https://get.arkade.dev | sh

# Install Oh-my-zsh
wget --no-check-certificate https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | sh
chsh -s /bin/zsh vagrant


